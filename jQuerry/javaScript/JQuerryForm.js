/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//javascript running
$(document).ready(function(){
    $('#submit-button').click(function(){
        //start of the function to be executed on clicking submit
        event.preventDefault();
        valid= true;
        //ensures first name is not left blank
        if ($('#first-name').val() === ''|| null){
            ($('#err-first-name').html("First name cannot be empty"));
            ($('#err-first-name').css("color","red"));
            ($('#err-first-name').css("font-size","small"));
            valid = false;}
        //to allow only alphabets in name 
        else
        {
            var reg = /^[a-zA-Z]*$/;
            if (!(reg.test($("#first-name").val())))
            ($('#err-first-name').html("Please enter alphabets only"));
            ($('#err-first-name').css("color","red"));
            ($('#err-first-name').css("font-size","small"));
        }
        //ensures last name is not left blank
        if($("#last-name").val()===""||null){
            ($('#err-last-name').html("Last name cannot be empty"));
            ($('#err-last-name').css("color","red"));
            ($('#err-last-name').css("font-size","small"));
            valid = false;
        }
        //to allow only alphabets in name
        else
        {
            var reg = /^[a-zA-Z]*$/;
            if (!(reg.test($("#last-name").val())))
            ($('#err-last-name').html("Please enter alphabets only"));
            ($('#err-last-name').css("color","red"));
            ($('#err-last-name').css("font-size","small"));
        }
        //to make DOB field compulsary
        if($('#date-of-birth').val() === ''|| null){
            ($('#err-dob').html("Please enter date of birth"));
            ($('#err-dob').css("color","red"));
            ($('#err-dob').css("font-size","small"));
            valid = false;
        }
        
        //to ensure phone no is not blank
        if($("#phone-no").val()===""||null){
            ($('#err-phone-no').html("Please enter a phone No."));
            ($('#err-phone-no').css("color","red"));
            ($('#err-phone-no').css("font-size","small"));
            valid = false;
        }
        //allows only digits 0-9 in phone no and special chars like (,),-,+ for formatting
        else
        {
            var reg = /^[0-9-+()]*$/;
            if (!(reg.test($("#phone-no").val())))
            ($('#err-phone-no').html("Please enter numerals only"));
            ($('#err-phone-no').css("color","red"));
            ($('#err-phone-no').css("font-size","small"));
        }
        //to ensure email is not left blank
        if ($('#e-mail').val() === ''|| null){
            ($('#err-email').html("E-mail cannot be empty"));
            ($('#err-email').css("color","red"));
            ($("#err-email").css("font-size","small"));
            valid = false;
        }
        //to password is not left blank
        if ($('#password').val() === ''|| null){
            ($('#err-password').html("please enter a password"));
            ($('#err-password').css("color","red"));
            ($("#err-password").css("font-size","small"));
            valid = false;
        }
        //to confirm-password is not left blank
        if ($('#confirm-password').val() === ''|| null){
            ($('#err-confirm-password').html("Please confirm your password"));
            ($('#err-confirm-password').css("color","red"));
            ($("#err-confirm-password").css("font-size","small"));
            valid = false;
        }
        //ensures password and confirm password do match
        if($("#password").val() !== ($("#confirmPassword"))){
            ($('#err-confirm-password').html("Passwords do not match"));
            ($('#err-confirm-password').css("color","red"));
            ($("#err-confirm-password").css("font-size","small"));
            valid = false;  
        };
    
    });
    
    //to prevent the user from enetering a future date
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    month = month + 1;
    if (month < 10)
    {
        month = '0' + month;
    }
    var year = date.getFullYear();
    var currentdate = (year + "-" + month + "-" + day);
    $("#dob").attr('max', currentdate);
    
    $("#phone-no").keydown(function(e)
    {
    //to add the international std code before phone no
        if (e.keyCode !== 8) {
             if ($(this).val().length === 0) {
                $(this).val($(this).val() + "+1(");
            }
            else if ($(this).val().length === 6) {
                $(this).val($(this).val() + ")-");
            } else if ($(this).val().length === 11) {
                $(this).val($(this).val() + "-");
            }
        }
    });
    //end of function to be executed on clicking submit
});